/**
 * http://stackoverflow.com/a/2671052
 * 
 * @author maerics
 * @author Jakub Vaněk
 */
public class Pos {
	/**
	 * X coordinate
	 */
	public final int x;
	/**
	 * Y coordinate
	 */
	public final int y;
	/**
	 * "None" position, used in BFS
	 */
	public static final Pos none = new Pos(-1,-1);
	
	
	
	/**
	 * Constructor
	 * @param x X coordinate
	 * @param y Y coordinate
	 */
	public Pos(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Convert Pos to string (x,y)
	 */
	@Override
	public String toString() {
		return Integer.toString(x) + "," + Integer.toString(y);
	}

	/**
	 * Modify position in specified orientation and length
	 * @param orig Pos to modify
	 * @param orient Orientation to move
	 * @param count Number to move
	 * @return Modified position
	 */
	public static Pos modify(Pos orig, int orient, int count)
	{
		switch(orient)
		{
		case Map.Orient.North:
			return new Pos(orig.x,orig.y-count);
		case Map.Orient.East:
			return new Pos(orig.x+count,orig.y);
		case Map.Orient.South:
			return new Pos(orig.x,orig.y+count);
		case Map.Orient.West:
			return new Pos(orig.x-count,orig.y);
			default:
				return orig;
		}
	}
	/**
	 * Generic equals()
	 */
	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof Pos)
		{
			Pos p = (Pos) obj;
			if (p.x == this.x && p.y == this.y)
				return true;
		}
		return false;
	}
	/**
	 * An experimental function for hash tables, possibly slow.
	 */
	@Override
	public int hashCode()
	{
		char[] ch = this.toString().toCharArray(); // Znaková notace
		int sum = (this.x+1)/(this.y+1); // Něco náhodného, řeší to prosté prohození souřadnic
		sum += (this.x+1)%(this.y+1); 
		for(char letter: ch)
			sum += letter; // Postupně sečteme ASCII hodnoty
		return sum; // vrátíme součet
	}
	/**
	 * Get positions adjacent to p
	 * @param p Base position
	 * @return Adjacent positions to p
	 */
	public static Pos[] adjacent(Pos p)
	{
		return new Pos[]{new Pos(p.x,p.y-1), // North
				new Pos(p.x+1,p.y), // East
				new Pos(p.x,p.y+1), // South
				new Pos(p.x-1,p.y)}; // West
	}
	/**
	 * Get orientation of next position against origin position; Poses must be adjacent
	 * @param original Original position (look from)
	 * @param next New position (look to)
	 * @return Map.Orient; if orig and next aren't adjacent, -1
	 */
	public static int directionOf(Pos original, Pos next)
	{
		return directionOf(original,next,1);
	}
	/**
	 * Get orientation of next position against origin position; Poses must be adjacent+count
	 * @param original Original position (look from)
	 * @param next New position (look to)from)
	 * @param count distance of Poses
	 * @return Map.Orient; if orig and next aren't adjacent, -1
	 */
	public static int directionOf(Pos original, Pos next, int count)
	{
		if(original.x == next.x && original.y-count == next.y) // Go up
			return Map.Orient.North;
		if(original.x+count == next.x && original.y == next.y) // Go left
			return Map.Orient.East;
		if(original.x == next.x && original.y+count == next.y) // Go down
			return Map.Orient.South;
		if(original.x-count == next.x && original.y == next.y) // Go right
			return Map.Orient.West;
		return 0;
		
	}
}