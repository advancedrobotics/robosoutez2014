import lejos.nxt.*;
import lejos.nxt.addon.*;

/**
 * Physical robot manipulation helping class.
 * @author Jakub Vaněk
 * @author Filip Smola
 */
public class Mechanics {

	// VARIABLES & CONSTANTS
	public int ACCEL = 2000;
	/**
	 * The speed for motors in degrees per seconds
	 */
	public int SPEED = 360;
	/**
	 * turning constant (wheel rotation with vehicle rotation)
	 */
	public double turn_constant;
	/**
	 * Straight constant (wheel perimeter)
	 */
	public float straigth_constant;
    /**
	 * Heading tracker for for turning accuracy enhancement
	 */
	private GyroDirectionFinder heading;
	/**
	 * The left motor
	 */
	private static final NXTRegulatedMotor left = Motor.A;
	/**
	 * The right motor
	 */
	private static final NXTRegulatedMotor right = Motor.B;
	/**
	 * Max turn error
	 */
	private static final int maxError = 5;
	/**
	 * Turn error fix rotation
	 */
	private static final int fixError = 5;
	// *Error above goes to functional
	/**
	 * Proportional constant
	 */
	private static final float Kp = 1f; // FIXME Doladit: tohle je potřeba odladit
	/**
	 * Where is plugged HiTechnic Gyro
	 */
	public static final SensorPort gyroPort = SensorPort.S2;
	
	
	// FUNCTIONS
	/**
	 * Init variables & constants
	 * 
	 * @param wheelDiameter
	 *            Wheel diameter
	 * @param trackWidth
	 *            Track width
	 * @param wheelBase
	 *            Wheelbase length
	 */
	public Mechanics(float wheelDiameter, float trackWidth, float wheelBase) {
		System.out.println("Computing turning constant...");
		turn_constant = (Math.sqrt(trackWidth * trackWidth + wheelBase
				* wheelBase)
				/ wheelDiameter)*1.2;

		System.out.println("Computing wheel perimeter...");
		straigth_constant = (float) (wheelDiameter * Math.PI);

		System.out.println("Initing & calibrating gyro...");
        GyroSensor gyroscope = new GyroSensor(gyroPort);
		long time1 = System.currentTimeMillis();
		gyroscope.recalibrateOffset();
		long time2 = System.currentTimeMillis();
		System.out.println("Taken: "+Long.toString(time2-time1)+"ms");
		heading = new GyroDirectionFinder(gyroscope, false);

		System.out.println("Setting speeds...");
		left.setSpeed(SPEED);
		right.setSpeed(SPEED);
		/*System.out.println("Heading zero-test:");
		while(heading.getDegreesCartesian()==0.0f)
			Delay.msDelay(20);
		System.out.println("Heading is now non-zero.");*/
	}

	/**
	 * Get turning angle
	 * 
	 * @param degree
	 *            Requested angle
	 * @return Angle for motors
	 */
	public int turn_degree(int degree) {
		return (int) (turn_constant * degree);
	}

	/**
	 * Get straight angle
	 * 
	 * @param distance
	 *            Distance in cm
	 * @return Angle for motors
	 */
	public int straight_degree(float distance) {
		return (int) (distance / straigth_constant * 360);
	}

    /**
     * Wait for rotations end
     * @param l Motor 1
     * @param r Motor 2
     * @return Success
     */
	public static boolean waitForRotators(Rotator l, Rotator r)
	{
		try{
			l.join();
			r.join();
		}catch(InterruptedException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Turn robot by specified degree, regulated by 3-zone regulator
	 * 
	 * @param goRight
	 *            Whether to go right (true) or go left (false)
	 * @param degree
	 *            How many degree to turn
	 */
	public void turn(boolean goRight, int degree){
		/*
		// reset trackeru úhlu
		heading.resetCartesianZero();
		// zjistit o kolik stupňů otočit kola
		int rotation = turn_degree(degree);
		// nastavit poloviční rychlost
		left.setSpeed(SPEED / 2);
		right.setSpeed(SPEED / 2);
		// pokud jedeme doprava
		if(goRight) // gyroskop je záporný
		{
			// začít otáčení motorů
			Rotator l = Rotator.rotate(left,rotation);
			Rotator r = Rotator.rotate(right,-rotation);
			waitForRotators(l, r);
			// odchylka
			int error;
			// Dokud abs(odchylka) je větší než maximální odchylka
			while(Math.abs(error = (int)heading.getDegreesCartesian()+degree)>maxError)
			{
				// pokud je měření menší než požadovaný úhel, nedotočili jsme se
				if(error > 0)
				{
					// Otočit motory pro opravení odchylky
					l = Rotator.rotate(left,turn_degree(fixError));
					r = Rotator.rotate(right,turn_degree(-fixError));
					waitForRotators(l, r);
				}
				else // měření větší než požadovaný úhel, přetočili jsme se
				{
					// Otočit motory pro opravení odchylky
					l = Rotator.rotate(left,turn_degree(-fixError));
					r = Rotator.rotate(right,turn_degree(fixError));
					waitForRotators(l, r);
				}
			}
		}
		else // jedeme doleva, gyroskop je kladný
		{
			// začít otáčení motorů
			Rotator l = Rotator.rotate(left,-rotation);
			Rotator r = Rotator.rotate(right,rotation);
			// počkat, dokud neskončí
			waitForRotators(l, r);
			// odchylka
			int error;
			// Dokud abs(odchylka) je větší než maximální odchylka
			while(Math.abs(error = degree-(int)heading.getDegreesCartesian())>maxError)
			{
				// pokud je měření menší než požadovaný úhel, nedotočili jsme se
				if(error > 0)
				{
					// Otočit motory pro opravení odchylky
					l = Rotator.rotate(left,turn_degree(-fixError));
					r = Rotator.rotate(right,turn_degree(fixError));
					waitForRotators(l, r);
				}
				else // měření větší než požadovaný úhel, přetočili jsme se
				{
					// Otočit motory pro opravení odchylky
					l = Rotator.rotate(left,turn_degree(fixError));
					r = Rotator.rotate(right,turn_degree(-fixError));
					waitForRotators(l, r);
				}
			}
		}
		// návrat na normální rychlost
		left.setSpeed(SPEED);
		right.setSpeed(SPEED);
		*/
        turnP(goRight,degree);

	}

	/**
	 * Turn robot by specified degree, regulated by proportional regulator
	 * and error memory
	 * @param goRight
	 *            Whether to go right (true) or go left (false)
	 * @param degree
	 *            How many degree to turn
	 */
	public void turnP(boolean goRight, int degree){
		// reset trackeru úhlu
		heading.resetCartesianZero();
		// zjistit o kolik stupňů otočit kola
		int rotation = turn_degree(degree);
		// nastavit poloviční rychlost
		left.setSpeed(SPEED / 2);
		right.setSpeed(SPEED / 2);
		if(goRight) // jedeme doprava, gyroskop záporný
		{
			// začít otáčení motorů
			Rotator l = Rotator.rotate(left,rotation);
			Rotator r = Rotator.rotate(right,-rotation);
			waitForRotators(l, r);
			int error;
			while(Math.abs(error = degree + (int) heading.getDegreesCartesian())>maxError) {
				// kladné nedotočení, záporné přetočení
				int fix = turn_degree((int) (Kp * error));// spočítat proporcionální opravu
				l = Rotator.rotate(left, fix); // otočit
				r = Rotator.rotate(right, -fix);
				waitForRotators(l, r);
			}
		}
		else // jedeme doleva, gyroskop kladný
		{
			// začít otáčení motorů
			Rotator l = Rotator.rotate(left,-rotation);
			Rotator r = Rotator.rotate(right,rotation);
			waitForRotators(l, r); // počkat, dokud neskončí
			int error;
			while(Math.abs(error = degree-(int)heading.getDegreesCartesian())>maxError) {
				// kladné nedotočení, záporné přetočení
				int fix = turn_degree((int) (Kp * error)); // spočátat proporcionální opravu
				l = Rotator.rotate(left, -fix); // otočit
				r = Rotator.rotate(right, fix);
				waitForRotators(l, r);
			}
		}
		// návrat na normální rychlost
		left.setSpeed(SPEED);
		right.setSpeed(SPEED);
	}

	/**
	 * Travel specified distance in specified time
	 * 
	 * @param centimeters
	 *            Travel distance in cm
	 * @param backward
	 *            Whether to go backward
	 * @param speed
	 *            Speed in centimeters/second
	 */
	public void travel(boolean backward, float centimeters, int speed) {
		travel(backward,centimeters,speed,false);
	}


	/**
	 * Travel specified distance in specified time
	 * 
	 * @param centimeters
	 *            Travel distance in cm
	 * @param backward
	 *            Whether to go backward
	 * @param speed
	 *            Speed in centimeters/second
	 * @param nonblock Whether this function should return after travel is complete or return immediataly
	 */
	public void travel(boolean backward, float centimeters, int speed, boolean nonblock) {
		// zjistit o kolik otočit motory
		int rotation = straight_degree(centimeters);
		// vypočítat úhlovou rychlost motorů
		int velocity = (int) ((speed/straigth_constant)*360);
		// nastavit rychlost
		left.setSpeed(velocity);
		right.setSpeed((int)(velocity*1.1));
		// pokud jedeme dozadu, točit dozadu
		if (backward)
			rotation *= -1;
		// začít točit motory
		Rotator l = Rotator.rotate(left,rotation);
		Rotator r = Rotator.rotate(right,rotation);
		// pokud chceme blokovat, počkat dokud jízda neskončí
		if(!nonblock)
			waitForRotators(l, r);
		// nastavit původní rychlost
		left.setSpeed(SPEED);
		right.setSpeed(SPEED);
	}

    /**
     * Sets angular SPEED to specified normal speed
     * @param speed Speed in centimeters/second
     */
	public void setSPEED(int speed)
    {
        SPEED = (int) ((speed/straigth_constant)*360);
    }

	/**
	 * Set motor acceleration (l+r.setAcceleration)
	 * @param accel Acceleration in degrees/s
	 */
	public void setAccel(int accel)
	{
		left.setAcceleration(accel);
		right.setAcceleration(accel);
	}
	/**
	 * Start traveling indefinite distance
	 * 
	 * @param backward
	 *            Whether to go backward
	 */
	public void travel(boolean backward) {
        right.setSpeed(SPEED);
        left.setSpeed(SPEED);
		// pokud jedeme dopředu
		if (!backward) { // točit dopředu
			right.forward();
			left.forward();
		} else { // jinak točit dozadu
			right.backward();
			left.backward();
		}
	}

    /**
     * Stop motors;
     * @since DO NOT USE TO STOP ASYNC ROTATOR! USE ONLY TO STOP travel(backward) !
     */
    public void stop()
    {
	    Thread r = new Thread(){
		    @Override
		    public void run()
		    {
			    right.suspendRegulation();
		    }
	    };
	    Thread l = new Thread(){
		    @Override
		    public void run()
		    {
			    left.suspendRegulation();
		    }
	    };
	    r.start();
	    l.start();
	    try {
		    r.join();
		    l.join();
	    }
	    catch(Exception e)
	    {
		    e.printStackTrace();
	    }
    }
	
	
	
	// INNER TYPES
	/**
	 * Ultrasonic sensor position "enumeration"
	 * @author Jakub Vaněk
	 */
	public static final class Direction
	{
		private Direction(){}
		public static final int Left = 0;
		public static final int Straight = 1;
		public static final int Right = 2;
	}
	public final static class Rotator extends Thread
	{
		private NXTRegulatedMotor motor;
		private int rotation = 360;
		public Rotator(NXTRegulatedMotor m, int degrees)
		{
			motor = m; // nastavíme motor
			rotation = degrees;	// a otočení
		}
		public static Rotator rotate(NXTRegulatedMotor m, int degrees)
		{
			Rotator r = new Rotator(m,degrees); // vytvoření nového vlákna
			r.start(); // start vlákna
			return r; // return vlákna
		}
		@Override
		public void run()
		{
			// otočit motorem
			motor.rotate(rotation);
		}
	}
}
