import lejos.nxt.*;
import lejos.util.Delay;
import java.util.HashMap;

/**
 * @author Jakub Vaněk
 * @author Filip Smola
 */
public class Main {
    // Constructor
	public Main()
	{
        visits = new HashMap<>();
        sonic = new UltrasonicSensor(sonicPort);
        touch = new TouchSensor(touchPort);
        touchB = new TouchSensor(SensorPort.S4);
        kill = new Killer(Button.ESCAPE,mech,Thread.currentThread()); // When we press ESCAPE, kill program
        kill.setPriority(Thread.currentThread().getPriority()+1);
        blik = new Blikac(Thread.currentThread());
        blik.setPriority(Thread.MIN_PRIORITY);
    }
	
    // Global constants
    public static final int TILE_LENGTH = 28; // How long is real tile
    public static final short US_LIMIT_COEF = 5; // Maximal distance in tiles that can US see
    public static final int MAX_VISITS = 3; // Maximal number of visits
    private static final int[] BFSREJECT = new int[]{Map.Tile.Unknown, Map.Tile.Wall}; // Which tiles must we avoid in final BFS
    // Mechanical constants
    public static final int SPEED = 30; // How fast must robot travel (cm/s)
    public static final int FORWARDSPEED = SPEED; // How fast must robot travel (cm/s)
    public static final int ACCEL = 2000; // Motor acceleration (degrees/s)
    public static final float WHEELDIAMETER = 8.3f; // Wheel diameter
    public static final float TRACKWIDTH = 14.8f; // Distance between front and back wheels
    public static final float WHEELBASE = 9.5f; // Distance between left and right wheels
    // Unused constants
    //public static final int US_LIMIT = US_LIMIT_COEF * TILE_LENGTH;
    //public static final int STOP_SPACE = 1;    //The distance to stop before wall
    //public static final float ROUND_LIMIT = 0.8f; // Rounding limit

    public static java.util.Map<Pos,Integer> visits; // Visits counter
    public Map map; // Virtual map

    public Mechanics mech; // Robot's physical interface
    public UltrasonicSensor sonic; // US for wall detection
    public static final SensorPort sonicPort = SensorPort.S1; // where is plugged US sensor
    public TouchSensor touch; // Touch sensor for "crashing" to walls
    public TouchSensor touchB;
    public static final SensorPort touchPort = SensorPort.S3; // where is plugged touch sensor

    public Blikac blik;
    public Killer kill;
	/**
	 * Main function
	 * @param args Nothing
	 */
	public static void main(String[] args) 
	{
		Main m = new Main(); // Create main
        m.init(); // Initialize all required stuff
        m.run(); // run!
	}

    /**
     * Initialize all parts
     */
    public void init()
    {
        kill.start();
        System.out.println("Anytime ESCAPE->exit");
        System.out.println("Initializing & calculating");
        mech = new Mechanics(WHEELDIAMETER,TRACKWIDTH,WHEELBASE);
        mech.setSPEED(SPEED);
        mech.setAccel(ACCEL);
        map = new Map();
    }
    public void load(int number)
    {
        String name = number+".txt";
        try {
            map.load(name);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    /**
     * Call sense to right and left, if there is any unknown in that direction
     */
    public void senseLR()
    {
        int origOr = map.curOrient;
        //check left
        if(map.beforeWall(false, Map.Tile.Unknown) > 0){
            sense(Mechanics.Direction.Left, false, origOr); // this could be slow
        }
        //check right
        if(map.beforeWall(true, Map.Tile.Unknown) > 0){
            sense(Mechanics.Direction.Right,false,origOr); // this could be slow
        }
        int diff = Map.Orient.difference(map.curOrient,origOr)*90;
        if(diff!=0)
            mech.turn(diff>0,Math.abs(diff));
        map.curOrient = origOr;
    }

    public void run()
    {
        //0) Start program
        blik.start();
        System.out.println("OTHER->run");
        Sound.playTone(700, 100);
        switch(Button.waitForAnyPress())
        {
		case Button.ID_ESCAPE:
			return; // exit
		default:
			break; // ENTER or other -> continue
		}


        //1) Explore
        while(map.curWalls < map.maxWalls){
            if(hitWall()) // Hit a wall; check whether turn is demanded
                turn(); // We are before wall, turn to correct direction
        }
        
        //2) Target all lights that are on
        Path p; // Path p
        // Get path to nearest light
        while((p = map.BFSearch(map.curPos, Map.Tile.LightOn, BFSREJECT))!=null) {
            p.follow(this, false); // follow this path
        }


        //3) Play some music :)
        playSound();

    }

    
    
    
    /**
     * Travel and sense until we hit the wall
     * @return Whether to turn.
     */
    public boolean hitWall() {
        int dist = sense(Mechanics.Direction.Straight); // Get distance
        boolean maxvisit = false; // Test if we already twice visited tile

        while(dist > 1){ // We are far away from wall
            senseLR(); //sense all sides
            Pos p = Pos.modify(map.curPos,map.curOrient,1);
            if(!visits.containsKey(p) || visits.get(p)<MAX_VISITS) {
                forward(); //move one tile forward
                int addVisit = visits.containsKey(map.curPos)?visits.get(map.curPos)+1:1;
                visits.put(map.curPos,addVisit);
                dist = sense(Mechanics.Direction.Straight); //prepare for next iteration
            }
            else
            {
                maxvisit = true;
                break;
            }
        }
        // One tile before hitting wall
        senseLR(); //sense all sides
        if(!maxvisit) {
            if(dist == 1) {
                //// FIXME Doladit: Toto pro náraz do zdi
                mech.travel(false); // travel until touch sensor is pressed
                while (!touch.isPressed() && !touchB.isPressed())
                    Delay.msDelay(10); // Wait until sensor is pressed
                mech.travel(true, 5, FORWARDSPEED);
                map.travel(1, false); // We travelled one
                //// FIXME Doladit: Nebo toto pro jen přijetí ke zdi
                //forward();
                senseLR(); // Sense all sides
            }
            return true;
        }
        else
        {
            Path p = map.BFSearch(map.curPos, Map.Tile.Unknown,new int[]{Map.Tile.Wall});
            p.follow(this,true);
            return false;
        }
    }

    
    
    
    /**
     * Turn to direction with more Unknowns
     */
    public void turn() {
        if(sidesFree()){    //open both sides
            //when orientation is vertical
            if(map.curOrient == Map.Orient.North || map.curOrient == Map.Orient.South){
                int west = 0; // sum of all unknown tiles
                int east = 0;

                //create non-inclusive division
                int lim = map.curPos.x;

                //go through each tile of the map and add it to the appropriate count if Unknown
                for(int x = 0; x < map.sizeX; x++){
                    for(int y = 0; y < map.sizeY; y++){
                        if(map.map[x][y] == Map.Tile.Unknown){
                            if(x < lim)
                                west++;
                            else if(x > lim)
                                east++;
                        }
                    }
                }

                //choose the direction to turn
                if(west > east){    //go west
                    if(map.curOrient == Map.Orient.North)
                        turnLeft();
                    else if(map.curOrient == Map.Orient.South)
                        turnRight();
                }else if(west < east){    //go east
                    if(map.curOrient == Map.Orient.North)
                        turnRight();
                    else if(map.curOrient == Map.Orient.South)
                        turnLeft();
                }else{
                    turnRight();
                }
                
                
            }else if(map.curOrient == Map.Orient.West || map.curOrient == Map.Orient.East){    //when orientation is horizonatal
                int north = 0;
                int south = 0;

                //create non-inclusive division
                int lim = map.curPos.y;

                //go through each tile of the map and add it to the appropriate count if Unknown
                for(int x = 0; x < map.sizeX; x++){
                    for(int y = 0; y < map.sizeY; y++){
                        if(map.map[x][y] == Map.Tile.Unknown){
                            if(y < lim)
                                north++;
                            else if(y > lim)
                                south++;
                        }
                    }
                }

                //choose the direction to turn
                if(north > south){    //go north
                    if(map.curOrient == Map.Orient.West)
                        turnRight();
                    else if(map.curOrient == Map.Orient.East)
                        turnLeft();
                    
                }else if(north < south){    //go south
                    if(map.curOrient == Map.Orient.West)
                        turnLeft();
                    else if(map.curOrient == Map.Orient.East)
                        turnRight();
                }else{
                    turnRight();
                }
            }
        }else if(!freeRight() && freeLeft())    //open on left
            turnLeft();
        else if(freeRight() && !freeLeft())    //open on right
            turnRight();
        else   //closed
        { // escape using shortest path
            Path p = map.BFSearch(map.curPos, Map.Tile.Unknown,new int[]{Map.Tile.Wall});
            if(p!=null)
                p.follow(this,true);
        }
    }
    private boolean freeLeft()
    {
        return !map.isLeft(Map.Tile.Wall); // If in map is free
    }
    private boolean freeRight()
    {
        return !map.isRight(Map.Tile.Wall);
    }
    private boolean sidesFree() {
        return freeLeft() && freeRight();
    }


    /**
     * Play ending sound
     */
    private void playSound() {
        int time = 200;
        //int wait = 10;
        int[] instrument = Sound.FLUTE;
        // First going up
        Sound.playNote(instrument, 500, time);
        //Delay.msDelay(wait);
        Sound.playNote(instrument, 700, time);
        //Delay.msDelay(wait);
        Sound.playNote(instrument, 900, time);
        //Delay.msDelay(wait);
        Sound.playNote(instrument, 1100, time);
        //Delay.msDelay(wait);

        // Second, higher going up
        Sound.playNote(instrument, 600, time);
        //Delay.msDelay(wait);
        Sound.playNote(instrument, 800, time);
        //Delay.msDelay(wait);
        Sound.playNote(instrument,1000, time);
        //Delay.msDelay(wait);
        Sound.playNote(instrument, 1200, time);
        //Delay.msDelay(wait);
        Sound.beepSequenceUp();
        Sound.beepSequence();
    }

    
    
    
    /**
     * Turns the robot 90 degrees to left
     */
	public void turnLeft(){
        turnLeft(1);
    }
    
    /**
     * Turns the robot 90 degrees to right
     */
    public void turnRight(){
        turnRight(1);
    }

    /**
     * Turns the robot 90 degrees to left
     */
    public void turnLeft(int count){
        mech.turn(false, count*90);
        map.turn(false, count);
    }

    /**
     * Turns the robot 90 degrees to right
     */
    public void turnRight(int count){
        mech.turn(true, count*90);
        map.turn(true, count);
    }
    
    /**
     * Moves one tile forward
     */
    public void forward(){
        forward(1);
    }
    
    /**
     * Moves one tile backward
     */
    public void backward(){
        backward(1);
    }


    /**
     * Moves one tile forward
     */
    public void forward(int count){
        mech.travel(false, TILE_LENGTH*count, FORWARDSPEED);
        map.travel(count, false);
    }

    /**
     * Moves one tile backward
     */
    public void backward(int count){
        mech.travel(true, TILE_LENGTH*count, FORWARDSPEED);
        map.travel(count, true);
    }


    public int sense(int dir)
    {
        return sense(dir,true, map.curOrient);
    }
    
    /**
     * Uses the US to write tiles in provided direction to map
     * @param dir The direction from Mechanics.Direction
     * @param immediateReturn Return robot immediately
     * @return Maximal number of free tiles
     */
    public int sense(int dir, boolean immediateReturn, int original){
        //stopped at wall
        // OLD CODE:
        /*
        if(dist < US_LIMIT){ // Correct detection
            int no = Math.round(dist / TILE_LENGTH);
            map.writeDetection(retVal = no, dir, true);
        }else if(dist == 255) // Too far or too near
            return 0;
        else if(dist >= US_LIMIT && dist < 255){    // Over the limit (wall too far), but not too far
            map.writeDetection(retVal = US_LIMIT_COEF, dir, false);
        }
        */
        /*
        //OLD NEW CODE:
        boolean canError = true; // If there can be error from too near wall
        int abs = Map.Orient.usEdit(map.curOrient, dir); // Absolute US orientation
        Pos pos = Pos.modify(map.curPos,abs,1); // Position of node in front of US
        int tile = map.map[pos.x][pos.y]; // Tile on that position
        if(tile == Map.Tile.Wall) // If that's wall
            return 0; // return zero free tiles
        else if(tile == Map.Tile.LightOn || tile == Map.Tile.LightOff) // There's at least one free tile
            canError = false; // No, no error can occur
        else if(tile == Map.Tile.Unknown) // If we don't know what tile is there
            canError = true; // Error could occur, try to correct it.
        int diff = Map.Orient.difference(map.curOrient,abs);
        mech.turn(diff>0,Math.abs(diff)*90);
        int dist = sonic.getDistance();
        if(dist == 255) // The side scenario
        {
            if(canError) // We are not sure if the sensor is not behind something which must be verified
            {
                if(dir == Mechanics.Direction.Straight)
                {
                    mech.travel(true,5,15);
                    dist = sonic.getDistance();
                    mech.travel(false,5,15);
                    if(dist == 255) // still 255, let's assume there are free tiles
                        map.writeDetection(US_LIMIT_COEF, dir, false);
                    else // value changed from 255 to something (small), write
                        return writeOkSense(dir, dist);
                }
                else if(dir == Mechanics.Direction.Left) // I hope the US is sufficiently far from wall; rotating robot is dangerous
                    map.writeDetection(US_LIMIT_COEF, dir, false);
                else if(dir == Mechanics.Direction.Right) // I hope the US is sufficiently far from wall; rotating robot is dangerous
                    map.writeDetection(US_LIMIT_COEF, dir, false);
                return US_LIMIT_COEF;
            }
            else { // We are sure that 255 means there isn't anything in front of us
                map.writeDetection(US_LIMIT_COEF, dir, false);
                return US_LIMIT_COEF;
            }
        }
        else // Good, not error -> we don't need limit
            return writeOkSense(dir, dist);
            */

        int abs = Map.Orient.usEdit(original, dir); // Absolute orientation
        Pos p = Pos.modify(map.curPos, abs, 1);
        int tile = map.map[p.x][p.y];
        if(tile == Map.Tile.Wall){
            return 0;
        }
        int rotate = Map.Orient.difference(map.curOrient, abs)*90;
        if(rotate!=0) {
            mech.turn(rotate > 0, Math.abs(rotate));
        }
        int usDist = sonic.getDistance();
        int retVal = writeSense(dir, usDist, tile);
        if(rotate!=0) {
            if(!immediateReturn)
                map.curOrient = abs;
            else
                mech.turn(rotate < 0, Math.abs(rotate));
        }
        return retVal;
    }

    /**
     * Helping function for sense(), when we are writing non-255 value.
     * @param dir US direction
     * @param dist Distance get from US
     * @return Number of free tiles
     */
    private int writeSense(int dir, int dist, int tile) {
        if(dist < 255) {
            int no = (int) Math.floor(((float) dist) / ((float) TILE_LENGTH));
            map.writeDetection(no, dir, true);
            return no;
        }else{
            if(tile == Map.Tile.LightOn || tile == Map.Tile.LightOff){
                map.writeDetection(US_LIMIT_COEF,dir,false);
                return US_LIMIT_COEF;
            }
            else if(tile == Map.Tile.Wall)
                return 0;
            else if(tile == Map.Tile.Unknown) {
                if (touch.isPressed()) {
                    map.writeDetection(0, dir, true);
                    return 0;
                } else {
                    map.writeDetection(US_LIMIT_COEF, dir, false);
                    return US_LIMIT_COEF;
                }
            }
        }
        return 0;
    }
}
