import lejos.util.Delay;

import javax.microedition.sensor.MindsensorsAccelerationSensorInfo;
import java.util.Queue;

/**
 * Path representation (replacement for Stack<Pos> or Queue<Pos>)
 * @author Jakub Vaněk
 */
public class Path {
	/**
	 * Path itself
	 */
	public Queue<Pos> path;

	/**
	 * Initialize the path
	 * @param path Path itself
	 */
	public Path(Queue<Pos> path) {
		this.path = path;
	}

	/**
	 * Follow this path and be interactive
	 * @param program Robot's main program (for it's fields)
	 */
	public void follow(Main program, boolean sense) {
		Pos node = (Pos) path.peek();
		if (node == program.map.curPos)
			path.pop();
		Pos lastNode = program.map.curPos;
        while (path.size() != 0) {
            if (sense) {
                int origOr = program.map.curOrient;
                program.sense(Mechanics.Direction.Straight, false, origOr);
                program.sense(Mechanics.Direction.Left,false, origOr);
                program.sense(Mechanics.Direction.Right, false, origOr);
                /*
                int diff = Map.Orient.difference(program.map.curOrient,origOr)*90;
                if(diff!=0)
                    program.mech.turn(diff>0,Math.abs(diff));
                program.map.curOrient = origOr;*/
            }
			node = (Pos) path.pop();
            if (node != lastNode) {

                int req = Pos.directionOf(lastNode, node);
                int direction = Map.Orient.difference(program.map.curOrient, req);
                Pos p = Pos.modify(program.map.curPos, req, 1);
                if(program.map.map[p.x][p.y] != Map.Tile.Wall) {
                    if(!sense) {
                        if (direction != 0) {
                            addWaitOrDo(new CMD(direction > 0 ? CMD.TURNRIGHT : CMD.TURNLEFT, Math.abs(direction)), program);
                            //program.map.turn(true, direction); // Pretty HACK, passing negative number for left-turn
                            //program.mech.turn(direction > 0, Math.abs(direction) * 90);
                        }
                        //program.forward();
                        addWaitOrDo(new CMD(CMD.FORWARD, 0), program);
                    }
                    else
                    {
                    if (direction != 0) {
                            program.map.turn(true, direction); // Pretty HACK, passing negative number for left-turn
                            program.mech.turn(direction > 0, Math.abs(direction) * 90);
                        }
                        program.forward();
                    }
                }
            }
            lastNode = node;
        }
    }
    private static Queue<CMD> fwds = new Queue<CMD>();
    private static void addWaitOrDo(CMD command, Main program)
    {
        if(command.TYPE == CMD.FORWARD) { // add, will be done later
            fwds.push(command);
            program.map.travel(1, false);
        } else if(command.TYPE == CMD.TURNLEFT) { // empty queue and turn
            emptyQueueAndTravel(program);

            program.map.turn(false, command.AMOUNT);
            program.mech.turn(false, command.AMOUNT * 90);
        } else if(command.TYPE == CMD.TURNRIGHT) { // empty queue and turn
            emptyQueueAndTravel(program);

            program.map.turn(true, command.AMOUNT);
            program.mech.turn(true, command.AMOUNT * 90);
        }

    }

    private static void emptyQueueAndTravel(Main program) {
        int count = countFWD();
        fwds.clear();
        int direction = program.map.curOrient;
        Pos p = Pos.modify(program.map.curPos, direction, 1);
        if(program.map.map[p.x][p.y] != Map.Tile.Wall) {
            program.mech.travel(false, Main.TILE_LENGTH * count, Main.FORWARDSPEED);
        }
        else
        {
            program.mech.travel(false); // travel until touch sensor is pressed
            while (!program.touch.isPressed() && !program.touchB.isPressed())
                Delay.msDelay(10); // Wait until sensor is pressed
            program.mech.travel(true, 5, Main.FORWARDSPEED);
        }
    }

    private static int countFWD()
    {
        int count = 0;
        Object[] jobArr = fwds.toArray();
        for(Object job:jobArr){
            if(((CMD)job).TYPE==CMD.FORWARD) {
                count++;
            }
            else {
                break;
            }

        }
        return count;
    }
    private static class CMD
    {
        public static final int FORWARD=0;
        public static final int TURNLEFT=1;
        public static final int TURNRIGHT=2;
        public int TYPE;
        public int AMOUNT;
        public CMD(int type, int amount)
        {
            TYPE = type;
            AMOUNT = amount;
        }
    }
}
