import lejos.nxt.*;
import lejos.util.Delay;
import java.util.*;

public class LastResort {
    // Constructor
	public LastResort()
	{
        touch = new TouchSensor(touchPort);
        kill = new Killer(Button.ESCAPE,mech,Thread.currentThread()); // When we press ESCAPE, kill program
        kill.setPriority(Thread.MIN_PRIORITY);
    }
	
    // Global constants
    public static final int TILE_LENGTH = 28; // How long is real tile
    
    // Mechanical constants
    public static final int SPEED = 40; // How fast must robot travel (cm/s)
    public static final int ACCEL = 3000; // Motor acceleration (degrees/s)
    public static final float WHEELDIAMETER = 8.3f; // Wheel diameter
    public static final float TRACKWIDTH = 14.8f; // Distance between front and back wheels
    public static final float WHEELBASE = 9.5f; // Distance between left and right wheels

    public Mechanics mech; // Robot's physical interface
    public TouchSensor touch; // Touch sensor for "crashing" to walls
    public static final SensorPort touchPort = SensorPort.S3; // where is plugged touch sensor
    
    public Killer kill;
    
	/**
	 * Main function
	 * @param args Nothing
	 */
	public static void main(String[] args) 
	{
		LastResort m = new LastResort(); // Create main
        m.init(); // Initialize all required stuff
        m.run(); // run!
	}

    /**
     * Initialize all parts
     */
    public void init()
    {
        kill.start();
        System.out.println("Anytime ESCAPE->exit");
        System.out.println("Initializing & calculating");
        mech = new Mechanics(WHEELDIAMETER,TRACKWIDTH,WHEELBASE);
        mech.setSPEED(SPEED);
        mech.setAccel(ACCEL);
    }

    private void run()
    {
        System.out.println("OTHER->run");
        Sound.playTone(700, 100);
        switch(Button.waitForAnyPress())
        {
		    case Button.ID_ESCAPE:
			    return; // exit
		    default:
			    break; // ENTER or other -> continue
		}
        
        Random r = new Random();
        
        //go to wall then turn
        while(!Button.ESCAPE.isDown()){
            mech.travel(false); // travel until touch sensor is pressed
            while (!touch.isPressed())
                Delay.msDelay(10); // Wait until sensor is pressed
            //mech.stop(); // Stop motor // I recently get that we don't need to stop motors, just do some rotate()
            mech.travel(true, 5, SPEED);
            
            //turn to random direction
            switch(Math.abs(r.nextInt(3))){
                case 0:
                    turnLeft();
                    break;
                case 1:
                    turnRight();
                    break;
                case 2:
                    turnRight(2);
                    break;
            }
        }

    }
    /**
     * Turns the robot 90 degrees to left
     */
	public void turnLeft(){
        turnLeft(1);
    }
    
    /**
     * Turns the robot 90 degrees to right
     */
    public void turnRight(){
        turnRight(1);
    }

    /**
     * Turns the robot 90 degrees to left
     */
    public void turnLeft(int count){
        mech.turn(false, count*90);
    }

    /**
     * Turns the robot 90 degrees to right
     */
    public void turnRight(int count){
        mech.turn(true, count*90);
    }
    
    /**
     * Moves one tile forward
     */
    public void forward(){
        forward(1);
    }
    
    /**
     * Moves one tile backward
     */
    public void backward(){
        backward(1);
    }


    /**
     * Moves one tile forward
     */
    public void forward(int count){
        mech.travel(false, TILE_LENGTH*count, SPEED);
    }

    /**
     * Moves one tile backward
     */
    public void backward(int count){
        mech.travel(true, TILE_LENGTH*count, SPEED);
    }
}
