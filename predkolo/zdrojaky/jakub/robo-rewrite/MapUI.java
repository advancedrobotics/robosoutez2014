import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.LCD;
import lejos.util.Delay;

import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Gauge;
import javax.microedition.lcdui.Graphics;

/**
 * Select map number
 */
public final class MapUI {
	public static int value = 1;
	/**
	 * Get number between
	 * @return
	 */
	public static Object lock = new Object();
	public static final int getNumber(final int maxNumber)
	{
		LCD.clear();
		LCD.drawString("Select map:",0,0);
		value = 1;
		LCD.drawString(value + "  ", 5, 1);
		Button.LEFT.addButtonListener(new ButtonListener() {
			@Override
			public void buttonPressed(Button b) {
				value--;
				if(value == 0)
				{
					value = maxNumber;
				}
				LCD.drawString(value + "  ", 5, 1);
			}

			@Override
			public void buttonReleased(Button b) {}
		});
		Button.RIGHT.addButtonListener(new ButtonListener() {
			@Override
			public void buttonPressed(Button b) {
				value++;
				if(value == maxNumber+1)
				{
					value = 1;
				}
				LCD.drawString(value + "  ", 5, 1);
			}

			@Override
			public void buttonReleased(Button b) {}
		});
		Button.ENTER.addButtonListener(new ButtonListener() {
			@Override
			public void buttonPressed(Button b) {
				synchronized (lock) {
					lock.notify();
				}
			}

			@Override
			public void buttonReleased(Button b) {}
		});
		try {
			synchronized (lock) {
				lock.wait();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return value;
	}
}
