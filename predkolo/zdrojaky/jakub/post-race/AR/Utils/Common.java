package AR.Utils;

/**
 * Obecné funkce, které se nehodí do specifické třídy.
 */
public final class Common
{
	/**
	 * Pomocná funkce pro hledání čísla v poli
	 * @param o Číslo k nalezení
	 * @param a Pole k prohledání
	 * @return Jestli je dané číslo v poli přítomen
	 */
	public static boolean aContainsO(int o, int[] a)
	{
		for(int element: a) // projdi všechny prvky pole
			if(element==o) // pokud se čísla shodují
				return true; // číslo nalezeno
		return false; // číslo nenalezeno
	}
	private Common(){} // zbytečný (ale nutný) konstruktor
}
