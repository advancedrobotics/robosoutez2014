package AR.Utils;

import AR.Const;
import lejos.nxt.Button; // tlačítko
import lejos.util.Delay; // sleep

/**
 * Zabíják programu. Při stisku tlačítka ukončí celý program. Sám pracuje jako nezávislé vlákno.
 * NUTNOST, jinak se při zaseknutí programu musí vyndavat baterie.
 */
public class Killer extends Thread {
	/**
	 * Vražedné tlačítko :D
	 */
    Button b;
	/**
	 * Rodičkovské vlákno
	 */
    Thread original;


    /**
     * Základní konstruktor, inicializuje zabijáka vražedným tlačítkem a rodičkovským vláknem.
     */
    public Killer(Thread orig)
    {
        this.b = Const.killButton;
        this.original = orig;
    }

	/**
	 * Práce vlákna
	 */
    public void run()
    {
        while(original.isAlive()) { // dokud pracuje rodičovské vlákno (opatření proti zombie zabijákovi)
            if(b.isDown()) { // pokud je stisknuto vražedné tlačítko
                System.out.println("Pressed " + b.toString() + "; exiting..."); // vypiš zprávu
                original.interrupt(); // ukonči rodičovské vlákno (teoreticky není potřeba)
                System.exit(0); // ukonči celý program (ukonči JVM)
            }
            Delay.msDelay(100); // čekej
        }
    }
}
