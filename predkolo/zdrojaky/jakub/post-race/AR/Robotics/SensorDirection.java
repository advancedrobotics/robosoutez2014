package AR.Robotics;

/**
 * Výčet směru ultrazvukového senzoru
 * @author Jakub Vaněk
 *
 */
public final class SensorDirection
{
	private SensorDirection(){}

	/**
	 * Doleva od robota
	 */
	public static final int Left = 0;
	/**
	 * Rovně s robotem
	 */
	public static final int Straight = 1;
	/**
	 * Doprava od robota
	 */
	public static final int Right = 2;
}
