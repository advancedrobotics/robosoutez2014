package AR.Sensors;

import AR.Const;
import AR.Controller;
import AR.Navigation.*;
import AR.Robotics.SensorDirection;

/**
 * Ultrazvukový senzor
 * TODO odpálit atomovkou
 */
public class Ultrasonic {
	/**
	 * Ovládací rozhraní
	 */
	private Controller ctl;

	/**
	 * Posune ovladač dál
	 * @param control ovládací rozhraní
	 */
	public Ultrasonic(Controller control)
	{
		this.ctl = control;
	}

	/**
	 * Zavolá <code>sense()</code> na obě strany, pokud jsou zde neznámé
	 */
	public void senseLR()
	{
		int origOr = ctl.map.curOrient; // záloha pozice
		if(ctl.map.beforeWall(false, TileType.Unknown) > 0){ // pokud jsou neznámé
			sense(SensorDirection.Left, false, origOr); // sensuj
		}
		//check right
		if(ctl.map.beforeWall(true, TileType.Unknown) > 0){ // pokud jsou neznámé
			sense(SensorDirection.Right,false,origOr); // sensuj
		}
		ctl.turnTo(origOr);
	}




	/**
	 * Pomocná funkce pro <code>sense()</code> pro zapisování výsledku
	 * @param dir směr ultrazvukového senzoru
	 * @param dist vzdálenost ze senzoru
	 * @param tile pole před robotem
	 * @return počet volných polí
	 */
	private int writeSense(int dir, int dist, int tile) {
		if(dist < 255) { // pokud nemáme error
			int no = (int) Math.floor(((float) dist) / ((float) Const.TILE_LENGTH)); // spočítej počet volných polí
			ctl.map.writeDetection(no, dir, true); // zapiš
			return no; // vrátí počet
		}else{ // máme error
			if(tile == TileType.LightOn || tile == TileType.LightOff){ // pokud je tam volno
				ctl.map.writeDetection(Const.US_LIMIT_COEF,dir,false); // zapsat max co lze vidět
				return Const.US_LIMIT_COEF;
			}
			else if(tile == TileType.Wall) // pokud je tam zeď
				return 0; // je to error
			else if(tile == TileType.Unknown) { // pokud nevíme
				if (ctl.touchPressed(false)) { // pokud je tlačítko zmáčklé
					ctl.map.writeDetection(0, dir, true); // zapsat zeď
					return 0;
				} else { // pokud není zmáčklé
					ctl.map.writeDetection(Const.US_LIMIT_COEF, dir, false); // zapsat max co lze vidět
					return Const.US_LIMIT_COEF;
				}
			}
		}
		return 0;
	}

	/**
	 * Použije ultrazvukový senzor ke zjištění polí v daném směru
	 * @param dir směr ultrazvukového senzoru
	 * @return počet volných polí
	 */
	public int sense(int dir)
	{
		return sense(dir,true, ctl.map.curOrient);
	}

	/**
	 * Použije ultrazvukový senzor ke zjištění polí v daném směru
	 * @param dir směr ultrazvukového senzoru
	 * @param immediateReturn vrátit robota do původní pozice
	 * @return Maximal number of free tiles
	 */
	public int sense(int dir, boolean immediateReturn, int original){
		int abs = MapOrientation.sensorEdit(original, dir); // směr senzoru
		Pos pos = Pos.modify(ctl.map.curPos, abs, 1); // co je před senzorem
		int tile = ctl.map.get(pos); // co je přesně před senzorem
		if(tile == TileType.Wall){ // pokud je tam zeď
			return 0; // vrať nulu
		}
		int rotate = MapOrientation.difference(ctl.map.curOrient, abs)*90; // o kolik se otočit
		if(rotate!=0) {
			ctl.mech.turn(rotate > 0, Math.abs(rotate)); // otočit
		}
		int usDist = ctl.sonic.getDistance(); // získat vzdálenost
		int retVal = writeSense(dir, usDist, tile); // zapsat
		if(rotate!=0) { // otočit zpět
			if(!immediateReturn)
				ctl.map.curOrient = abs;
			else
				ctl.mech.turn(rotate < 0, Math.abs(rotate));
		}
		return retVal;
	}
}
