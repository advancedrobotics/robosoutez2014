package AR.Navigation;

/**
 * Výčet směru
 * @author Jakub Vaněk
 */
public class MapOrientation {
	private MapOrientation() {
	}

	/**
	 * Směr nahoru (jakoby k posluchárně)
	 */
	public static final int North = 0;
	/**
	 * Směr doprava (jakoby k oknu)
	 */
	public static final int East = 1;
	/**
	 * Směr dolů (jakoby k plátnu)
	 */
	public static final int South = 2;
	/**
	 * Směr doleva (jakoby ke zdi)
	 */
	public static final int West = 3;

	/**
	 * Otočí směr o daný počet posunů doprava
	 *
	 * @param actual Původní směr
	 * @param edit   Počet posunů po směru hodin. ručiček; při záporném počtu počet posunů proti směru hodin. ručiček
	 * @return Změněný směr
	 */
	public static int edit(int actual, int edit) {
		return (actual + edit + 4) % 4;// výpočet; +4 je kvůli nezápornému modulu
	}

	/**
	 * Získá směr senzoru vzhledem k mapě
	 *
	 * @param actual Směr robota
	 * @param usPos  Směr senzoru vzhledem k robotovi
	 * @return Směr senzoru vzhledem k mapě
	 * @see AR.Robotics.SensorDirection
	 */
	public static int sensorEdit(int actual, int usPos) {
		return edit(actual, usPos - 1); // převodní vztah mezi otočením a směrem senzoru je -1
	}

	/**
	 * Získá optimální otočení z původního do nového směru
	 *
	 * @param first  Původní směr
	 * @param second Nový směr
	 * @return Optimální otočení (kladné je po směru hodin. ručiček)
	 */
	public static int difference(int first, int second) {
		if (first == second) // pokud už tímto směrem jsme
			return 0; // netoč se
		while (first != 0 && second != 0) // dokud není jeden ze směrů nulový
		{
			first--; // dekrementuj oba dva
			second--;
		}
		if (first < second) // normálně první rozhodnutí - toč po směru hodinových ručiček
		{
			if (second == 1) // 90° zatáčka po směru hodin. ručiček
				return 1;
			if (second == 2) // 180° zatáčka po směru hodin. ručiček
				return 2;
			if (second == 3) // 90° zatáčka proti směru hodin. ručiček
				return -1;
		} else // normálně první rozhodnutí - toč proti směru hodinových ručiček
		{
			if (first == 1) // 90° zatáčka proti směru hodin. ručiček
				return -1;
			if (first == 2) // 180° zatáčka po směru hodin. ručiček
				return 2;
			if (first == 3) // 90° zatáčka po směru hodin. ručiček
				return 1;
		}
		return 0; // chyba, netočit
	}
}
