package AR.Robotics;

import AR.Const;
import lejos.nxt.*;
import lejos.nxt.addon.*;

/**
 * Rozhraní pro fyzický pohyb robota
 * TODO kompletně překopat, viz FIXME.txt
 * @author Jakub Vaněk
 */
public class Mechanics {

	// --- PROMĚNNÉ A KONSTANTY ---
	/**
	 * Rychlost motorů ve stupních za sekundu
	 */
	public int SPEED = 360;
	/**
	 * Zatáčecí konstanta (poměr mezi otočením kol a otočením robota)
	 */
	public double turn_constant;
	/**
	 * Přímočará konstanta (obvod kol)
	 */
	public float straigth_constant;
    /**
	 * Záznamník gyroskopu
	 */
	private GyroDirectionFinder heading;
	/**
	 * Levý motor
	 */
	private NXTRegulatedMotor left;
	/**
	 * Pravý motor
	 */
	private NXTRegulatedMotor right;
	/**
	 * Maximální odchylka při zatáčení
	 */
	private static final int maxError = 5;
	/**
	 * Proporcionální konstanta korekce zatáčení
	 */
	private static final float Kp = 1f;
	
	
	// --- KONSTRUKTOR ---
	/**
	 * Inicializuje zařízení a konstanty
	 * TODO konstanty by se mohly cachovat na interní disk, přestože to není to nejpomalejší
	 */
	public Mechanics() {
		right = new NXTRegulatedMotor("A");
	}

	// --- PODPŮRNÉ FUNKCE ---
	/**
	 * Získá otočení kol pro otočení robota o daný úhel
	 * 
	 * @param degree
	 *            Požadovaný úhel
	 * @return Úhel pro motory
	 */
	private int turn_degree(int degree) {return 0;}                                           // ale bude to násobení rotace robota poměrem otočení kol a otočení robota

	/**
	 * Získá otočení kol pro ujetí dané vzdálenosti
	 * 
	 * @param distance
	 *            Vzdálenost v cm
	 * @return Úhel pro motory
	 */
	private int straight_degree(float distance) {return 0;}

	/**
	 * Nastaví rychlost levého a pravého motoru
	 * @param speed Úhlová rychlost motorů
	 */
	private void putSpeed(float speed)
	{

	}

	// --- VEŘEJNÉ API ---
	/**
	 * Otočí robota o daný úhel a zkoriguje jej pomocí proporcionálního regulátoru
	 *
	 * @param goRight
	 *            Jestli zatáčet doprava (CW) (true) nebo doleva (CCW) (false)
	 * @param degree
	 *            O jaký úhel se otočit
	 */
	public void turn(boolean goRight, int degree){

	}

	/**
	 * Ujede danou vzdálenost danou rychlostí
	 * 
	 * @param centimeters
	 *            Vzdálenost v cm
	 * @param backward
	 *            Jestli jet dopředu (false) nebo dozadu (true)
	 * @param speed
	 *            Rychlost v cm/s
	 */
	public void travel(boolean backward, float centimeters, int speed) {

		right.trigger();
	}

	/**
     * Nastaví úhlovou rychlost SPEED na danou rovinnou rychlost
     * @param speed Rychlost v cm/s
     */
	public void setSPEED(int speed)
    {

    }


	/**
	 * Nastaví úhlové zrychlení motorů (l+r.setAcceleration)
	 * @param accel Zrychlení ve °/s
	 */
	public void setAccel(int accel)
	{

	}


	/**
	 * Spustí motory a nechá je běžet
	 * 
	 * @param backward
	 *            Jestli jet dopředu (false) nebo dozadu (true)
	 */
	public void travel(boolean backward) {
		right.trigger();
	}
}
