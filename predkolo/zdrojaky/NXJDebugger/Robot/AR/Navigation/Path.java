package AR.Navigation;

import AR.Const;
import AR.Controller;
import AR.Robotics.SensorDirection;
import lejos.util.Delay; // sleep

import java.util.LinkedList;
import java.util.Queue; // fronta (FIFO)

/**
 * Trasa (náhrada za Stack<Pos> nebo Queue<Pos>)
 * @author Jakub Vaněk
 */
public class Path {
	/**
	 * Interní reprezentace trasy, fronta souřadnic
	 */
	public Queue<Pos> path;

	/**
	 * Inicializuje trasu seřazeným seznamem (frontou) bodů (souřadnic)
	 * @param path Path itself
	 */
	public Path(Queue<Pos> path) {
		this.path = path;
	}
	/**
	 * Jeď po cestě a případně snímej vzdálenosti
	 * @param ctl Instance ovladače (pro přístup k mnoha rozhraním např. Mechanics, Map)
	 */
	public void follow(Controller ctl, boolean sense) {
		Pos node = (Pos) path.peek(); // nahlédni na první bod cesty, musí to být bod,
										// kde je robot nebo který je vedle něj
		if (node == ctl.map.curPos) // pokud je robot na tomto bodě
			path.poll(); // vyhoď tento bod
		Pos lastNode = ctl.map.curPos; // současný bod je tam, kde je robot
        while (path.size() != 0) { // dokud máme cestu
            if (sense) { // pokud máme snímat; při soutěži nepoužito; vypíná cachování
                int origOr = ctl.map.curOrient; // ulož původní směr robota
                ctl.sonicctl.sense(SensorDirection.Straight, false, origOr); // podívej se rovně, nevracej se
                ctl.sonicctl.sense(SensorDirection.Left, false, origOr); // podívej se doleva vzhledem
																		// k původnímu směru, nevracej se
                ctl.sonicctl.sense(SensorDirection.Right, false, origOr); // podívej se doprava vzhledem
																		// k původnímu směru, nevracej se
                /* // zde by bylo otočení zpět,ale to lze udělat níže
                int diff = MapOrientation.difference(program.map.curOrient,origOr)*90;
                if(diff!=0)
                    program.mech.turn(diff>0,Math.abs(diff));
                program.map.curOrient = origOr;*/
            }
			node = (Pos) path.poll(); // získá budoucí bod
            if (node != lastNode) { // ochrana proti duplicitním bodům

                int req = Pos.directionOf(lastNode, node); // kam má robot mířit
                int direction = MapOrientation.difference(ctl.map.curOrient, req); // o kolik se máme otočit
                if(ctl.map.get(node) != TileType.Wall) { // pokud nejedeme do zdi
                    if(!sense) { // pokud nemáme snímat
                        if (direction != 0) { // pokud máme zatočit
							// ulož si do cache: zatočení
                            addWaitOrDo(new CMD(direction > 0 ? CMD.TURNRIGHT : CMD.TURNLEFT, // doleva/doprava
											Math.abs(direction)), ctl);// o kolik, program
                        }
                        addWaitOrDo(new CMD(CMD.FORWARD, 0), ctl); // ulož si do cache: jeď dopředu
                    }
                    else // pokud máme snímat
                    {
						if (direction != 0) { // pokud máme zatočit
                            ctl.map.turn(true, direction); // zatoč v mapě
											// HACK, pro zatočení doleva záporné číslo
                            ctl.mech.turn(direction > 0, Math.abs(direction) * 90); // zatoč fyzicky
											// HACK nemožný, Mechanics.turnP() závisí na správnosti booleanu
                        }
                        ctl.forward(); // jeď fyzicky i v mapě dopředu
                    }
                } // pokud jedeme do zdi tak se cesta nejspíš hodně pokazí
            }
            lastNode = node; // už jsme popojeli, minulá budoucnoust = současnost
        }
    }


	/**
	 * Fronta posunů dopředu; vynechání OOP - static
	 */
    private static Queue<CMD> fwds = new LinkedList<>();

	/**
	 * Přidá do cache příkaz pro jízdu dopředu nebo vykoná cache a otočí se
	 * @param command Příkaz
	 * @param ctl Instance robotova programu
	 */
    private static void addWaitOrDo(CMD command, Controller ctl)
    {
        if(command.TYPE == CMD.FORWARD) { // pokud máme jet dopředu
            fwds.add(command); // přidej do fronty, uděláme později
	        ctl.map.travel(1, false); // posuň se v mapě
        } else if(command.TYPE == CMD.TURNLEFT) { // pokud máme zatočit doleva
            emptyQueueAndTravel(ctl); // vykoná cache

	        ctl.map.turn(false, command.AMOUNT); // zatočí v mapě doleva
	        ctl.mech.turn(false, command.AMOUNT * 90); // fyzicky zatočí
        } else if(command.TYPE == CMD.TURNRIGHT) { // pokud máme zatočit doprava
            emptyQueueAndTravel(ctl); // vykoná cache

	        ctl.map.turn(true, command.AMOUNT); // zatočí v mapě doprava
	        ctl.mech.turn(true, command.AMOUNT * 90); // fyzicky zatočí
        }

    }

	/**
	 * Spočítá a vyprázdní cache, popojede o daný počet polí
	 * @param ctl Instance robotova programu
	 */
    public static void emptyQueueAndTravel(Controller ctl) {
        int count = countFWD(); // spočítej cache
        fwds.clear(); // vyčisti cache
        int direction = ctl.map.curOrient; // současný směr
        Pos p = Pos.modify(ctl.map.curPos, direction, 1); // co je před námi v mapě
        // POZOR! Do mapy se zapisuje už při Path.follow(), proto posun pouze o 1
        if(ctl.map.get(p) != TileType.Wall) { // pokud před námi není zeď
            ctl.mech.travel(false, Const.TILE_LENGTH * count, Const.FORWARDSPEED); // jeď danou vzdálenost
        }
        else // pokud před sebou máme zeď
        {
            ctl.mech.travel(false); // zapni motor
            while (!ctl.touch.isPressed() && !ctl.touchB.isPressed()) // dokud nejsou zmáčklá obě tlačítka
                Delay.msDelay(10); // čekej
            ctl.mech.travel(true, 5, Const.FORWARDSPEED); // couvni
        }
    }

	/**
	 * Spočítá počet popojezdů v cache
	 * @return počet popojezdů
	 */
    private static int countFWD()
    {
        int count = 0; // čítač
        Object[] jobArr = fwds.toArray(); // převede frontu na pole
        for(Object job:jobArr){ // postupně projde všechny prvky
            if(((CMD)job).TYPE==CMD.FORWARD) { // pokud máme jet dopředu
                count++; // přičti
            } else {
                break; // ukonči smyčku
            }

        }
        return count; // vrať součet
    }

	/**
	 * Příkaz
	 */
    private static class CMD
    {
	    /**
	     * Typ dopředu
	     */
        public static final int FORWARD=0;
	    /**
	     * Typ doleva
	     */
        public static final int TURNLEFT=1;
	    /**
	     * Typ doprava
	     */
        public static final int TURNRIGHT=2;
	    /**
	     * Typ příkazu
	     */
        public int TYPE;
	    /**
	     * Počet; pouze u zatáčení
	     */
        public int AMOUNT;

	    /**
	     * Základní konstruktor
	     * @param type Typ příkazu
	     * @param amount Počet (jen u otáčení)
	     */
        public CMD(int type, int amount) // konstruktor
        {
            this.TYPE = type;
            this.AMOUNT = amount;
        }
    }
}
