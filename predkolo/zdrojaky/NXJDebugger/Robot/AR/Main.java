package AR;

import AR.Robotics.*;
import AR.Navigation.*;
import lejos.nxt.*;
import lejos.util.Delay;

/**
 * Hlavní třída, mozek robota
 */
public class Main {
	/**
	 * Konstruktor
	 */
	public Main()
	{
    }

	/**
	 * Bloky k vyhnutí při koncovém BFS
	 */
    private static final int[] BFSREJECT = new int[]{TileType.Unknown, TileType.Wall};
	/**
	 * Ovladač robota
	 */
	public Controller ctl;
	/**
	 * Hlavní funkce (s prohledáváním, pro přednačtenou mapu se používá <code>mapMain.main()</code>)
	 * @param args Nothing
	 */
	public static void main(String[] args) 
	{
		Main m = new Main(); // vytvoření instance programu
        m.init(); // inicializace a výpočty
		System.out.println("HOTOVO!");
        m.run(); // spuštění!
	}

    /**
     * Inicializace, výpočty konstant atd.
     */
    public void init()
    {
        System.out.println("ESCAPE -> exit");
        System.out.println("Prosim cekejte..");
        Mechanics mech = new Mechanics(); // mechanika
        mech.setSPEED(Const.SPEED); // nastavení konstant
        mech.setAccel(Const.ACCEL);
        Map map = new Map(); // mapa
	    ctl = new Controller(map,mech); // vytvoření ovladače pro program a ostatní třídy
    }

	/**
	 * Načte mapu podle čísla
	 * @param number číslo mapy
	 */
    public void load(int number)
    {
        String name = number+".txt"; // soubor je jednoduše číslo+.txt
        try {
            ctl.map.load(name); // posunutí dál, toto je v rámci reference, tudíž Controller je ovlivněn
        }catch(Exception e){
            e.printStackTrace();
        }
    }

	/**
	 * Hlavní algoritmus
	 */
    public void run()
    {
        //0) Start programu
        System.out.println("jedeme");
        Sound.playTone(700, 100);

		// TODO toto už není potřeba
		// FIXME dojetí ke zdi a pak točení vylučuje prostředek hřiště
        //1) Objevování mapy
        while(ctl.map.curWalls < ctl.map.maxWalls){
            hitWall(); // Dojet ke zdi
	        turn(); // Otočit někam
        }
        
        //2) Dojetí ke všem svítícím světlům
        Path p; // Cesta
        // Získat cestu k nejbližšímu světlu
        while((p = ctl.map.BFSearch(ctl.map.curPos, TileType.LightOn, BFSREJECT))!=null) {
            p.follow(ctl, false); // a následovat ji
        }
	    // TOTALFAIL toto zde nebylo
	    // odjet naplánované popojezdy
        Path.emptyQueueAndTravel(ctl);

    }

    
    
    
    /**
     * Dojet a <code>sense</code>ovat až ke zdi
     */
    public void hitWall() {
	    int dist = ctl.sonicctl.sense(SensorDirection.Straight); // Získat vzdálenost

	    while (dist > 1) { // dokud jsme daleko od zdi
		    ctl.sonicctl.senseLR(); // prozkoumej strany
		    ctl.forward(); // popojeď dopředu
		    dist = ctl.sonicctl.sense(SensorDirection.Straight); // připrav se na další iteraci
	    }
	    // jeden blok od zdi
	    ctl.sonicctl.senseLR(); // prozkoumej okolí
	    if (dist == 1) {
		    ctl.mech.travel(false); // jeď ke zdi
		    while (!ctl.touchPressed(false))
			    Delay.msDelay(10); // čekej dokud nenarazíme
		    ctl.mech.travel(true, 5, Const.FORWARDSPEED); // popojeď dozadu
		    ctl.map.travel(1, false); // posuň se v mapě
		    ctl.sonicctl.senseLR(); // prozkoumej strany
	    }
    }

    
    
    
    /**
     * Otočit se do strany kde je více neznámých bloků
     */
    public void turn() {
        if(ctl.map.sidesFree()){ // pokud jsou obě strany otevřené
            // když je směr vertikální
            if(ctl.map.curOrient == MapOrientation.North || ctl.map.curOrient == MapOrientation.South){
                int west = 0, east = 0; // součet všech neznámých

                // vytvoř neinkluzivní hranici
                int lim = ctl.map.curPos.x;

                // projdi celou mapu a přidej neznámé na správnou stranu
                for(int x = 0; x < ctl.map.sizeX; x++){
                    for(int y = 0; y < ctl.map.sizeY; y++){
                        if(ctl.map.get(x,y) == TileType.Unknown){
                            if(x < lim)
                                west++;
                            else if(x > lim)
                                east++;
                        }
                    }
                }

                // zjisti kam se otočit
                if(west > east)    // jeď doleva
                    ctl.turnTo(MapOrientation.West);
                else if(west < east) // jeď doprava
	                ctl.turnTo(MapOrientation.East);
                else
	                ctl.turnRight();

            }else if(ctl.map.curOrient == MapOrientation.West || ctl.map.curOrient == MapOrientation.East){
                            // když je směr horizontální
                int north = 0, south = 0;

	            // vytvoř neinkluzivní hranici
                int lim = ctl.map.curPos.y;

                // projdi celou mapu a přidej neznámé na správnou stranu
                for(int x = 0; x < ctl.map.sizeX; x++){
                    for(int y = 0; y < ctl.map.sizeY; y++){
                        if(ctl.map.get(x,y) == TileType.Unknown){
                            if(y < lim)
                                north++;
                            else if(y > lim)
                                south++;
                        }
                    }
                }

	            // zjisti kam se otočit
                if(north > south)    // jeď doleva
                    ctl.turnTo(MapOrientation.North);
                    
                else if(north < south) // jeď doprava
	                ctl.turnTo(MapOrientation.South);

            }
        }else if(!ctl.map.freeRight() && ctl.map.freeLeft())    // volno vlevo
	        ctl.turnLeft();
        else if(ctl.map.freeRight() && !ctl.map.freeLeft())    // volno vpravo
	        ctl.turnRight();
        else   // uzavřeno
        { // uteč k nejbližší neznámé
            Path p = ctl.map.BFSearch(ctl.map.curPos, TileType.Unknown,new int[]{TileType.Wall});
            if(p!=null) // FIXME tady se může najet do zdi...
                p.follow(ctl,true);
        }
    }

}
