package nxjsim.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Main extends Application {
	public void start(Stage window) throws Exception {
		GridPane inside = FXMLLoader.load(getClass().getResource("Control.fxml"));
		window.setTitle("NXT test simulator");
		window.setResizable(false);
		window.setScene(new Scene(inside));
		window.show();
	}
	public static void main(String[] args) {
		Application.launch(Main.class, args);
	}
}
