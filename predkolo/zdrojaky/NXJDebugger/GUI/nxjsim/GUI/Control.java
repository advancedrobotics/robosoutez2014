package nxjsim.GUI;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import AR.Navigation.*;

import java.net.URL;
import java.util.ResourceBundle;

public class Control implements Initializable {
	@FXML
	public Canvas canvas;
	@FXML
	public Button button;
	@FXML
	public Canvas blik;
	@FXML
	public Button start;
	@FXML
	public ComboBox<Integer> mapNumber;

	public Thread mainThread;
	public AR.Main main;
	final Object pressLock = new Object();
	public GraphicsContext context;
	public SimEndpoint backend;
	final int tileSize = 30;
	@Override
	public void initialize(URL url, ResourceBundle b)
	{
		context = canvas.getGraphicsContext2D();
		context.setFont(Font.font(context.getFont().getFamily(), FontWeight.BOLD, 20));
		ObservableList<Integer> list = FXCollections.observableArrayList(1,2,3,4,5,6,7,8);

		button.setOnAction((actionEvent)->exit());
		mapNumber.setItems(list);
		mapNumber.getSelectionModel().select((int) 3);
		start.setOnAction(event -> {
			initSim(mapNumber.getSelectionModel().getSelectedItem());
			mainThread.start();
			start.setDisable(true);
		});
		button.setDisable(true);
	}
	public void exit()
	{
		button.setDisable(true);
		if(mainThread!=null && mainThread.isAlive()) {
			while(mainThread.isAlive()) {
				try{mainThread.interrupt();}
				catch(Exception e)
				{System.err.println("interrupt exception: exit(): "+e.getMessage());}
			}
		}
		mainThread = null;
		main = null;
		lejos.nxt.Button.listeners.clear();
		lejos.nxt.NXTRegulatedMotor.listeners.clear();
		lejos.nxt.TouchSensor.listeners.clear();
		lejos.nxt.UltrasonicSensor.listeners.clear();
		lejos.nxt.addon.GyroDirectionFinder.listeners.clear();
		backend = null;
	}
	public void pressButton() {
		/*
		button.setStyle("-fx-font-weight: bold");
		button.setText("PRESS ME");
		synchronized(pressLock){
			try {
				pressLock.wait();
			}catch(Exception ignored){}
		}
		button.setStyle("-fx-font-weight: normal");
		button.setText("Button");*/
	}

	public void blik(int level) {
		Color c = new Color(((float)level)/100f,0f,0f,1f);
		GraphicsContext context1 = blik.getGraphicsContext2D();
		context1.setFill(c);
		context1.fillRect(0,0,canvas.getWidth(),canvas.getHeight());
	}

	public void draw() {
		try{
		final int robotSize = Math.round(tileSize * 0.6f);
		for(int y = 0; y < main.ctl.map.sizeY; y++) {
			for (int x = 0; x < main.ctl.map.sizeX; x++) {
				int tile = main.ctl.map.get(x,y);
				switch(tile){
					case TileType.LightOff:
						context.setFill(Color.GRAY);
						break;
					case TileType.LightOn:
						context.setFill(Color.YELLOW);
						break;
					case TileType.Unknown:
						context.setFill(Color.MAGENTA);
						break;
					case TileType.Wall:
						context.setFill(Color.BLACK);
						break;
				}
				context.fillRect(x*tileSize,y*tileSize,tileSize,tileSize);
			}
		}
		int xStart = main.ctl.map.curPos.x*tileSize+(tileSize-robotSize)/2;
		int yStart = main.ctl.map.curPos.y*tileSize+(tileSize-robotSize)/2;
		context.setFill(Color.LIME);
		context.fillRect(xStart, yStart, robotSize, robotSize);
		context.setFill(Color.RED);
		switch(main.ctl.map.curOrient)
		{
			case MapOrientation.North:
				context.fillText("↑",xStart,yStart+16);
				break;
			case MapOrientation.East:
				context.fillText("→",xStart,yStart+16);
				break;
			case MapOrientation.South:
				context.fillText("↓",xStart,yStart+16);
				break;
			case MapOrientation.West:
				context.fillText("←",xStart,yStart+16);
				break;
		}
		}catch(Exception e){
			System.err.println("draw() exception:"+e.getMessage());
		}
	}
	public void initSim(int number)
	{
		button.setDisable(false);
		backend = new SimEndpoint(this);
		lejos.nxt.Button.addListener(backend);
		lejos.nxt.NXTRegulatedMotor.addListener(backend);
		lejos.nxt.TouchSensor.addListener(backend);
		lejos.nxt.UltrasonicSensor.addListener(backend);
		lejos.nxt.addon.GyroDirectionFinder.addListener(backend);
		main = new AR.Main();
		main.init();
		try{main.load(number);}catch(Exception ignored){}
		Platform.runLater(this::draw);
		if(mainThread!=null && mainThread.isAlive())
			mainThread.interrupt();
		mainThread = new Thread()
		{
			@Override
			public void run() {
				try{
				main.run();
				Platform.runLater(Control.this::exit);}
				catch(Exception e)
				{e.printStackTrace();}
			}
		};
		mainThread.setDaemon(true);
		Platform.runLater(() -> start.setDisable(false));
	}
}
