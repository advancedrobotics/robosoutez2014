package lejos.nxt;
import nxjsim.core.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Motor emulation.
 * @author Jakub Vaněk
 */
public class NXTRegulatedMotor {
    public static List<EventListener> listeners = new ArrayList<>(); // motor listeners
    public String name; // name of the motor
    public int tacho = 0; // tachometer
    public int speed = 0; // speed
    public int state = MotorState.STOPPED; // motor state
    public static final int SLEEP = 500;
    public static void addListener(EventListener l) // add listener (GUI)
    {
        listeners.add(l);
    }
    private void SendEvent(MotorEvent e, int value) // send event notification to listeners
    {
        for(EventListener l: listeners)
            l.MotorModified(this,e,value);

    }
    public NXTRegulatedMotor(String name) // constructor
    {
        this.name = name;
    }
	public void trigger()
	{
		SendEvent(MotorEvent.FORWARD,0);
	}
    public void rotate(int angle) // rotate
    {
        tacho += angle;
        SendEvent(MotorEvent.ROTATE, angle);
        try {
            Thread.sleep(SLEEP);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void forward() // forward
    {
        state = MotorState.FORWARD;
        SendEvent(MotorEvent.FORWARD, 0);
    }
    public void backward() // backward
    {
        state = MotorState.BACKWARD;
        SendEvent(MotorEvent.BACKWARD, 0);
    }
    public void stop() // stop
    {
        state = MotorState.STOPPED;
        SendEvent(MotorEvent.STOP, 0);
    }
    public void setSpeed(int speed) // set motor speed
    {
        SendEvent(MotorEvent.SPEED,this.speed = speed);
    }
    public static enum MotorEvent // what event occured
    {
        ROTATE,
        FORWARD,
        BACKWARD,
        SPEED,
        STOP
    }
    public static final class MotorState // state of the motor
    {
        private MotorState()
        {}
        public static final int FORWARD = 0;
        public static final int BACKWARD = 1;
        public static final int STOPPED = 2;
    }
}
