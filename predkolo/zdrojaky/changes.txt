1.10.2014
	JakubVanek - založení repozitáře
9.10.2014
	Filip Smola - přidán soubor MovementThread.java (základní kostra pohybového vlákna)
11.10.2014
	JakubVanek - MovementThread.java - 1. podpůrné mechanické metody, FORWARD_PWR=180 _stupňů za sekundu_
	                                 - 2. přidání startovních konstant, vylepšení zatáčení
12.10.2014
	JakubVanek - vytvoření vlastní větve - složka jakub
